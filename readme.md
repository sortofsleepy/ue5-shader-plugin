Shader Plugin
===
An example of how to add an HLSL shader as a plugin

Description
==
Unreal Engine, though powerful, does make a couple of things kind of a pain in the ass. When it comes to custom HLSL shader code, by 
default, you're limited to using Custom Nodes. If you want to use separate shader files, you either have to bundle them inside your instance of Unreal or use a plugin.

Initial Setup
===
* So first steps is to just make a plugin. 
* Add your shader file to the plugin folder. You should also re-reference the path to your shader folder to make it a bit simpler to find later on
* [This video](https://www.youtube.com/watch?v=V3BVsYV7ge0) goes into how to do both. 

Once that's done
===
* Once you have that made, you essentially have two different ways to tackle things. 
* You can keep a file of functions that you can include inside of a custom node using the `#include` directive. This does however come with some potential drawbacks
  * You're essentially limited to creating pixel effects as you need to return a `float3` or `float4` from the code. 
  * You won't have access to things like `SV_Position` or any other things you might want from a vertex shader.
* The other method is to tie directly into the render pass system like this plugin does. 

What this plugin does
===
This plugin provides a sample of how to ingest a `UTextureRenderTarget2D` that you can create in the editor. Your shader output will get written to that render target which you can then
incorperate into a material of your choice. You will however need a C++ based actor or tweak your game module a bit in order to make use of this functionality as there are functions to call in order to render to the render target. 

How to use
===
````

// in your actor constructor / BeginPlay

// reference the render target you want to write to
rt = LoadObject<UTextureRenderTarget2D>(nullptr, TEXT("/Game/Sketches/planes/metaball/MetaballTarget"));

// Call plugin. Should automatically update as it's tied to rendering thread.
FKShaderPluginModule::Get().BeginRendering(rt)


````

Shader Uniforms / Parameters
===
Shader Parameters are not shown in this example for simplicity, but in order to use shader Parameters, 

* First, declare your uniforms 
```
// FMyShaderParameters is the name of your parameter struct */
BEGIN_SHADER_PARAMETER_STRUCT(FMyShaderParameters, /** MODULE_API_TAG */)
	SHADER_PARAMETER(FVector2f, ViewportSize)
END_SHADER_PARAMETER_STRUCT()
```

* Then, in your shader class, call `SHADER_USE_PARAMETER_STRUCT` and declare a `using` directive typed to `FParameters` and set it to be equal to the name of your parameter struct.

For example 

```
class FMetaballShaderPS : public FGlobalShader
{
public:

	//DECLARE_GLOBAL_SHADER(FMetaballShaderPS)
	DECLARE_EXPORTED_SHADER_TYPE(FMetaballShaderPS, Global, /*MYMODULE_API*/);
	SHADER_USE_PARAMETER_STRUCT(FMetaballShaderPS,FGlobalShader);


	// Assign an FParameters type to the shader--either with an inline definition or using directive.
	using FParameters = FMyShaderParameters;
};
```

* When you go to set your shader parameters, it looks something like this 

```
	FMetaballShaderPS::FParameters params;
	params.ViewportSize = FVector2f(1024.0,1024.0);
	SetShaderParameters(RHICmdList,fs,fs.GetPixelShader(),params);
	
```

Notes
===
* This has been tested with v5.2.0
* Maybe it's possible and I just haven't found the documentation or sample for it but note that you basically can only add new shaders into the main global pool of shaders.

Credits
===
This is largely based on [@temaran's](https://github.com/Temaran/UE4ShaderPluginDemo) example with some slight adjustments to make it compatible with UE 5
