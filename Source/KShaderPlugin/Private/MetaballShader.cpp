﻿
#include "MetaballShader.h"
#include "CommonRenderResources.h"
#include "Engine/TextureRenderTarget2D.h"

IMPLEMENT_SHADER_TYPE(, FMetaballShaderVS, TEXT("/KMetaballs/MyShader.USF"), TEXT("MainVS"), SF_Vertex);
IMPLEMENT_SHADER_TYPE(, FMetaballShaderPS, TEXT("/KMetaballs/MyShader.USF"),TEXT("MainPS"),SF_Pixel);

//! Simple full screen quad for rendering 
class FullScreenBuffer: public FVertexBuffer
{
public:
	void InitRHI() override
	{
		TResourceArray<FFilterVertex,VERTEXBUFFER_ALIGNMENT> Vertices;
		Vertices.SetNumUninitialized(6);

		Vertices[0].Position = FVector4f(-1.0f, 1.0f, 0.0f, 1.0f);
		Vertices[0].UV = FVector2f(0, 0);

		Vertices[1].Position = FVector4f(1, 1, 0, 1);
		Vertices[1].UV = FVector2f(1, 0);

		Vertices[2].Position = FVector4f(-1, -1, 0, 1);
		Vertices[2].UV = FVector2f(0, 1);

		Vertices[3].Position = FVector4f(1, -1, 0, 1);
		Vertices[3].UV = FVector2f(1, 1);

		FRHIResourceCreateInfo CreateInfo(TEXT("FS_QIUAD"));
		CreateInfo.ResourceArray = &Vertices;
		VertexBufferRHI = RHICreateVertexBuffer(Vertices.GetResourceDataSize(), BUF_Static, CreateInfo);
	}
};

	TGlobalResource<FullScreenBuffer> GSimpleScreenVertexBuffer;

	
void MetaballShader::RenderShader(FRHICommandList& RHICmdList, UTextureRenderTarget2D* rt)
{

	FRHIRenderPassInfo RenderPassInfo(rt->GetRenderTargetResource()->GetRenderTargetTexture(), ERenderTargetActions::Clear_Store);
	RHICmdList.BeginRenderPass(RenderPassInfo, TEXT("METABALLS_SHADER"));

	auto smap= GetGlobalShaderMap(GMaxRHIFeatureLevel);

	TShaderMapRef<FMetaballShaderVS> vs(smap);
	TShaderMapRef<FMetaballShaderPS> fs(smap);
	
	FGraphicsPipelineStateInitializer GraphicsPSOInit;
	RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
	GraphicsPSOInit.BlendState = TStaticBlendState<>::GetRHI();
	GraphicsPSOInit.RasterizerState = TStaticRasterizerState<>::GetRHI();
	GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<false, CF_Always>::GetRHI();
	GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GFilterVertexDeclaration.VertexDeclarationRHI;
	GraphicsPSOInit.BoundShaderState.VertexShaderRHI = vs.GetVertexShader();
	GraphicsPSOInit.BoundShaderState.PixelShaderRHI = fs.GetPixelShader();
	GraphicsPSOInit.PrimitiveType = PT_TriangleStrip;


	SetGraphicsPipelineState(RHICmdList,GraphicsPSOInit,0,EApplyRendertargetOption::ForceApply);

	// TODO setup pixel shaders if necessary

	// Draw the quad 
	RHICmdList.SetStreamSource(0, GSimpleScreenVertexBuffer.VertexBufferRHI, 0);
	RHICmdList.DrawPrimitive(0,2,1);

	RHICmdList.EndRenderPass();
};
