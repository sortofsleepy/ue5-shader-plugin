// Copyright Epic Games, Inc. All Rights Reserved.

#include "KShaderPlugin.h"
#include "ShaderPluginhader.h"
#include "RHI.h"
#include "RHICommandList.h"
#include "RenderGraphBuilder.h"
#include "RenderTargetPool.h"
#include "Runtime/Core/Public/Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"

#define LOCTEXT_NAMESPACE "FKShaderPluginModule"

void FKShaderPluginModule::StartupModule()
{
	OnPostResolvedSceneColorHandle.Reset();
	
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	FString BaseDir = FPaths::Combine(FPaths::ProjectPluginsDir(),TEXT("KShaderPlugin"));
	FString ModuleShaderDir = FPaths::Combine(BaseDir,TEXT("Shaders"));
	AddShaderSourceDirectoryMapping(TEXT("/KShaderPlugin"),ModuleShaderDir);
	
}

void FKShaderPluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	EndRendering();
}

void FKShaderPluginModule::BeginRendering(UTextureRenderTarget2D * rt)
{
	if(OnPostResolvedSceneColorHandle.IsValid())
	{
		return;
	}

	RenderTarget = rt;
	const FName RendererModuleName("Renderer");
	IRendererModule* RendererModule = FModuleManager::GetModulePtr<IRendererModule>(RendererModuleName);
	if (RendererModule)
	{
		OnPostResolvedSceneColorHandle = RendererModule->GetResolvedSceneColorCallbacks().AddRaw(this, &FKShaderPluginModule::RenderCallback);
	}
}

void FKShaderPluginModule::RenderCallback(FRDGBuilder&builder, const FSceneTextures& SceneContext)
{

	FRHICommandListImmediate& RHICmdList = builder.RHICmdList;

	// Make sure we're in the rendering thread
	check(IsInRenderingThread());

	// Run the metaball shader
	ShaderPluginhader::RenderShader(RHICmdList, RenderTarget);

	
}

void FKShaderPluginModule::DebugTest()
{
	
}


void FKShaderPluginModule::EndRendering()
{
	if(!OnPostResolvedSceneColorHandle.IsValid())
	{
		return;
	}

	const FName RendererModuleName("Renderer");
	IRendererModule* RendererModule = FModuleManager::GetModulePtr<IRendererModule>(RendererModuleName);
	if (RendererModule)
	{
		RendererModule->GetResolvedSceneColorCallbacks().Remove(OnPostResolvedSceneColorHandle);
	}

	OnPostResolvedSceneColorHandle.Reset();
}





#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FKShaderPluginModule, KShaderPlugin)