// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TextureRenderTarget2D.h"
#include "RenderGraphResources.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

class SHADERPLUGIN_API FKShaderPluginModule : public IModuleInterface
{

public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	void BeginRendering(UTextureRenderTarget2D * rt);
	void EndRendering();

	static inline FKShaderPluginModule&Get()
	{
		return FModuleManager::LoadModuleChecked<FKShaderPluginModule>(TEXT("KShaderPlugin"));
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("KShaderPlugin");
	}
protected:
	FDelegateHandle OnPostResolvedSceneColorHandle;
	void RenderCallback(FRDGBuilder&builder, const FSceneTextures&  SceneContext);

	UTextureRenderTarget2D * RenderTarget;
};
