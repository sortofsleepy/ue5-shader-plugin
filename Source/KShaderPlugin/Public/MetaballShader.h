﻿
#pragma once

#include "CoreMinimal.h"
#include "PipelineStateCache.h"
class FMetaballShaderPS : public FGlobalShader
{
public:
	DECLARE_EXPORTED_SHADER_TYPE(FMetaballShaderPS, Global, /*MYMODULE_API*/);

	FMetaballShaderPS(){}
	FMetaballShaderPS(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
	   : FGlobalShader(Initializer)
	{
		
	}
};

class FMetaballShaderVS : public FGlobalShader
{
public:
	DECLARE_EXPORTED_SHADER_TYPE(FMetaballShaderVS, Global, /*MYMODULE_API*/);

	FMetaballShaderVS(){}
	FMetaballShaderVS(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
	   : FGlobalShader(Initializer)
	{
		
	}
};


class MetaballShader
{
public:

	static void RenderShader(FRHICommandList &RHICmdList, UTextureRenderTarget2D* rt );

};

